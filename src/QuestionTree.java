import java.io.PrintStream;
import java.util.Scanner;

public class QuestionTree {
    private QuestionNode root;
    private Scanner scanner;

    public QuestionTree() {
        root = new QuestionNode("Кит");
        scanner = new Scanner(System.in);
    }

    public void read(Scanner input) {
        while (input.hasNext()) {
            root = readHelper(input);
        }
    }

    private QuestionNode readHelper(Scanner input) {
        String type = input.nextLine();
        String data = input.nextLine();
        QuestionNode root = new QuestionNode(data);

        if (type.contains("Q:")) {
            root.yesNode = readHelper(input);
            root.noNode = readHelper(input);
        }
        return root;
    }

    public void write(PrintStream output) {
        if (output == null) {
            throw new CustomException("Что-то пошло не так!");
        }
        writeTree(root, output);
    }

    private void writeTree(QuestionNode root, PrintStream output) {
        if (isAnswerNode(root)) {
            output.println("A:");
            output.println(root.data);
        } else {
            output.println("Q:");
            output.println(root.data);
            writeTree(root.yesNode, output);
            writeTree(root.noNode, output);
        }
    }

    public void askQuestions() {
        root = askQuestions(root);
    }

    private QuestionNode askQuestions(QuestionNode current) {
        if (isAnswerNode(current)) {
            if (yesTo("Ты загадал " + current.data + "?")) {
                System.out.println("Круто! Я молодец!");
            } else {
                System.out.println("Какое животное ты задагал? ");
                QuestionNode answer = new QuestionNode(scanner.nextLine());
                System.out.println("Чем загаданное животное отличается от предложенного животного:");
                String question = scanner.nextLine();
                if (yesTo("Какой ответ для придуманого животного?")) {
                    current = new QuestionNode(question, answer, current);
                } else {
                    current = new QuestionNode(question, current, answer);
                }
            }
        } else {
            if (yesTo(current.data)) {
                current.yesNode = askQuestions(current.yesNode);
            } else {
                current.noNode = askQuestions(current.noNode);
            }
        }
        return current;
    }

    public boolean yesTo(String aff) {
        System.out.println(aff + " (y/n) ");
        String response = scanner.nextLine().trim().toLowerCase();
        while (!response.equals("y") && !response.equals("n")) {
            System.out.println("Выберите ответ да либо нет!");
            System.out.println(aff + " (y/n) ");
            response = scanner.nextLine().trim().toLowerCase();
        }
        return response.equals("y");
    }

    private boolean isAnswerNode(QuestionNode node) {
        return (node.yesNode == null || node.noNode == null);
    }
}
