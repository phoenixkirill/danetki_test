import java.io.*;
import java.util.*;

public class Main {

    public static final String QUESTION_FILE = "/Users/kirill/IdeaProjects/Daneyki_test/src/question";

    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("Привет!" + "\n");

        QuestionTree questions = new QuestionTree();
        if (questions.yesTo("Начнем игру?")) {
            questions.read(new Scanner(new File(QUESTION_FILE)));
        }

        do {
            System.out.println("Загадай животное, а я попробую угадать...");
            questions.askQuestions();
            System.out.println();
        } while (questions.yesTo("Сыграем еще раз?"));
        questions.write(new PrintStream(QUESTION_FILE));
    }
}
